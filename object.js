
//Transfer value from variable to object
// let name = "ABC";
// let age = 18;
// let object = { name, age }

//Transfer value from object to variable
let object = {
    name: "ABC",
    age: 18,
    birthday: { day: 12, month: 9, year: 2000 },
    scores: [10, 9, 8, 7],
    fly() {
        //this.name === object.name
        console.log(`${this.name} can fly`);
    }
};

//Convert object to JSON
let json = JSON.stringify(object);

//Convert JSON to object
object = JSON.parse(json);

let { name, age } = object;
object.name = "BCD"
let object2 = Object.assign({}, object);

//Check property exits in object
'name' in object;
object.name !== undefined;
object.hasOwnProperty('name');

//Get values from object
Object.values(object);

//Remove property of object
delete object.name

//Get keys from object
Object.keys(object).forEach(key => {
    console.log(key);
})


let time = {
    days,
    hours,
    minutes,
    seconds,
    convertTimeToSecond() {
        return this.days * 24 * 60 ** 2 + this.hours * 60 ** 2 + this.minutes * 60 + this.seconds;
    }
};

function convertTimeToSecond(day, hours, minutes, ) {

} a

console.log(time.convertTimeToSecond());


