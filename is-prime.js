
function isPrime(number) {
    if (number <= 2) {
        return true;
    } else {
        for (let j = 2; j < number; j++) {
            if ((number % j) == 0) {
                return false
            }
        }
        return true;
    }
}

let temp = 0;
let arr = [1, 2, 5, 12, 6, 24, 7, 13, 28, 59, 23, 109];
let arrPrime = [];

for (let i = 0; i < arr.length; i++) {
    if(isPrime(arr[i])){
        arrPrime.push(arr[i]);
    }
    if (isPrime(arr[i]) && temp < arr[i]) {
        temp = arr[i];
    }
}
console.log(temp);
console.log(arrPrime);

