class Personal {
    constructor(ID, fullName, email, birthday, position, seniority) {
        this.ID = ID;
        this.fullName = fullName;
        this.email = email;
        this.birthday = birthday;
        this.position = position;
        this.seniority = seniority;
    }
    get age() {
        let today = new Date();
        let bday = new Date(this.birthday);
        let age = today.getFullYear() - bday.getFullYear(); //default value of age
        if (today.getMonth() < bday.getMonth()) {
            age--;
        } else if (today.getMonth() == bday.getMonth() && today.getDate() < bday.getDate()) {
            age--;
        }
        return age;
    }
    get salary() {
        let position = {
            manager: 1000,
            secterary: 400,
            employee: 300
        }
        let sal = position['security'];
        if (this.seniority < 12) {
            return sal * 1.2;
        }
        if (this.seniority < 24) {
            return sal * 2;
        }
        if (this.seniority < 36) {
            return sal * 3.4
        }
        return sal * 4.5
    }
    static highestSalary(arr) {
        let max = 0;
        let dem = 0;
        arr.forEach((element, index) => {
            if (max < element.salary) {
                max = element.salary;
                dem = index;
            }
        });
        return arr[dem]
    }
}
let personal1 = new Personal("1", "ng van", "ngvan@gmail.com", "10/23/1997", "manager", 3); // M M/dd/yyyy
let personal2 = new Personal("1", "ng An", "ngan@gmail.com", "10/25/1997", "secterary", 6);
let personal3 = new Personal("1", "ng no", "nan@gmail.com", "12/1/1997", "employee", 6);
// console.log(personal1.salary);
// console.log(Personal.highestSalary([personal1,personal2,personal3]));

Personal.prototype.gender = "male";

console.log(personal3.gender);

// c = a > b ? a - b : b > a ? b - a : a + b;

let abc = () => {
    console.log('abc');
}

console.log(abc());


