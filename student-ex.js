
// check(a){
//     Object.values(this.scores).forEach(element => {
//         if(element < a){
//             return false
//         }
//     });
//     return true;
// }
class Student {
    constructor(ID, firstName, lastName, scores) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.scores = scores;
    }
    get averageScore() {
        let values = Object.values(this.scores)
        return values.reduce((a, b) => a + b) / values.length
    }
    compare(a) {     
        for(let score of Object.values(this.scores)) {
            if (score < a) return true;
        }
        return false;
    }
    get studentTitle() {
        if (this.compare(3.5) || this.averageScore < 5) {
            return "Bad Student";
        } else if(this.averageScore < 6.5 || this.compare(5)) {
            return "Average Student";
        } else if (this.averageScore < 8 || this.compare(6.5)) {
            return "Good Student";
        } else {
            return "Excellent student"
        }
    }

    static bestStudents(students) {
        // Sort list of students by average score property
        students.sort((a, b) => (a.averageScore < b.averageScore ? 1 : b.averageScore < a.averageScore ? -1 : 0))
        // Slice the sorted list from start index to index of 50 percent of length
        return students.slice(0, Math.ceil(students.length * 0.5))
    }

}

let person1 = new Student("123", "a", "nguyeb", { math: 2.6, physical: 3.5, chemistry: 2.9})
let person2 = new Student("12433", "Pi", "nguyeb", { math: 1, physical: 2, chemistry: 3.4})
let person3 = new Student("12gf3", "Hanh", "nguyeb", { math: 6, physical: 3, chemistry: 3.9})
let person4 = new Student("12wvd3", "Trang", "nguyeb", { math: 2.6, physical: 1.5, chemistry: 5})
let person5 = new Student("124sgfg3", "Thu", "nguyeb", { math: 4.6, physical: 3.5, chemistry: 2.9})

console.log(person1.averageScore);
console.log(Student.bestStudents([person1, person2, person3, person4, person5]));
