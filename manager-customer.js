var Account = /** @class */ (function () {
    function Account(accountID, customer, memberType, balance) {
        this.accountID = accountID;
        this.customer = customer;
        this.memberType = memberType;
        this.balance = balance;
    }
    Account.prototype.getCustomerInfo = function () {
        return this.customer.name + "(" + this.accountID + ") have balance: $" + this.balance;
    };
    Account.prototype.deposit = function (amount) {
        this.balance += amount;
        return "Deposit +$" + amount + " success, your current balance is $" + this.balance;
    };
    Account.prototype.withdraw = function (amount) {
        if (amount > this.balance) {
            return "Withdraw failed! Your money lower than amount you want to withdraw";
        }
        this.balance -= amount;
        return "Withdraw -$" + amount + " success, your current balance is $" + this.balance;
    };
    return Account;
}());
var discount;
(function (discount) {
    discount[discount["premium"] = 0.2] = "premium";
    discount[discount["gold"] = 0.15] = "gold";
    discount[discount["silver"] = 0.1] = "silver";
})(discount || (discount = {}));
var Visit = /** @class */ (function () {
    function Visit(accountID) {
        this.serviceExpense = 0;
        this.accountID = accountID;
    }
    Visit.prototype.setServiceExpense = function (money, discountRate) {
        this.serviceExpense = money - money * discountRate;
    };
    Visit.prototype.getServiceExpense = function () {
        return this.serviceExpense;
    };
    return Visit;
}());
var account1 = new Account(1, { name: 'Nguyen Van A', gender: 'male', age: 18 }, 'gold', 1000);
var visit1 = new Visit(1);
visit1.setServiceExpense(100, discount.premium);
console.log(visit1.getServiceExpense());
