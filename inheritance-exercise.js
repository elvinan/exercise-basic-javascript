class Person {
    constructor(firstName,lastName, birthday, gender){
        this.firstName = firstName
        this.lastName = lastName
        this.birthday = birthday
        this.gender = gender
    }
    get age(){
         let bday= new Date(this.birthday);
         let today=new Date();
         let age = today.getFullYear()-bday.getFullYear();
         if(today.getMonth()<bday.getMonth()){
             age--;
         }
         else if(today.getMonth()==bday.getMonth()&& today.getDate()<bday.getDate()){
             age--;
         }
         return age;
         
    }
}
class Employer extends Person{
    constructor(firstName, lastName, birthday, gender,position, company, workHours)
    {
        super(firstName, lastName, birthday, gender)
        this.position = position
        this.company = company
        this.workHours = workHours
    }
    get salary(){
         let position={
            manager: 10,
            secterary: 7,
            employee: 4 
        } 
         return position[this.position]*this.workHours
    }

}
class Investors extends Person{
    constructor(firstName, lastName, birthday, gender,numberOfStopcks){
        super(firstName, lastName, birthday, gender)
        this.numberOfStopcks = numberOfStopcks
    }
    percentOfStocks(allStock){
        return this.numberOfStopcks/allStock
    }
}
let uyen= new Person("uyen","nguyen","1/1/1998","female")
let my= new Employer("my","nguyen","2/2/1997","male","manager","abc",160)
let lieu= new Investors("lieu","phan","3/3/1998","female",32)
console.log(my.age);
console.log(uyen.salary);
console.log(my.salary);
console.log(lieu.percentOfStocks(43));

// >>21
// 1600
// 0.7441860465116279


