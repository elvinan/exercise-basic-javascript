interface Customer {
    name: string;
    gender: string;
    age: number;
}

class Account {
    accountID: number;
    customer: Customer;
    memberType: string;
    balance: number;

    constructor(
        accountID: number,
        customer: Customer,
        memberType: string,
        balance: number
    ) {
        this.accountID = accountID;
        this.customer = customer;
        this.memberType = memberType;
        this.balance = balance;
    }


    getCustomerInfo(): string {
        return `${this.customer.name}(${this.accountID}) have balance: $${this.balance}`
    }

    deposit(amount: number): string {
        this.balance += amount;
        return `Deposit +$${amount} success, your current balance is $${this.balance}`
    }

    withdraw(amount: number): string {
        if (amount > this.balance) {
            return `Withdraw failed! Your money lower than amount you want to withdraw`
        }
        this.balance -= amount;
        return `Withdraw -$${amount} success, your current balance is $${this.balance}`
    }
}

enum discount {
    premium = 0.2,
    gold = 0.15,
    silver = 0.1,
}

class Visit {
    accountID: number;
    serviceExpense: number = 0;
    constructor(accountID: number) {
        this.accountID = accountID;
    }

    setServiceExpense(money: number, discountRate: number) {
        this.serviceExpense = money - money * discountRate;
    }
    getServiceExpense() {
        return this.serviceExpense;
    }
}

let account1 = new Account(1, { name: 'Nguyen Van A', gender: 'male', age: 18 }, 'gold', 1000);
let visit1 = new Visit(1);

visit1.setServiceExpense(100, discount.premium);
console.log(visit1.getServiceExpense());


