
function test() {
    var msg = {
        type: 'RSA PRIVATE KEY',
        procType: {
          version: 4,
          type: 'ENCRYPTED'
        },
        dekInfo: {
          algorithm: 'AES-256-CBC',
          parameters: 'kfjdfj'
        },
        body: 'cipher.output.getBytes()'
      };
      
      var rval = '-----BEGIN ----------\r\n'
      var header;
      if(msg.procType) {
          header = {
              name: 'Proc-Type',
              values: [String(msg.procType.version), msg.procType.type]
          }
          rval += foldHeader(header)
      }
      if (msg.dekInfo) {
          header = {name: 'DEK-Info', values: [msg.dekInfo.algorithm]}
          if (msg.dekInfo.parameters) {
              header.values.push(msg.dekInfo.parameters)
          }
          rval += foldHeader(header)
      }
      console.log(rval);
}

function foldHeader(header) {
    var rval = header.name + ': ';
  
    // ensure values with CRLF are folded
    var values = [];
    var insertSpace = function(match, $1) {
      return ' ' + $1;
    };
    for(var i = 0; i < header.values.length; ++i) {
      values.push(header.values[i].replace(/^(\S+\r\n)/, insertSpace));
    }
    rval += values.join(',') + '\r\n';
  
    // do folding
    var length = 0;
    var candidate = -1;
    for(var i = 0; i < rval.length; ++i, ++length) {
      if(length > 65 && candidate !== -1) {
        var insert = rval[candidate];
        if(insert === ',') {
          ++candidate;
          rval = rval.substr(0, candidate) + '\r\n ' + rval.substr(candidate);
        } else {
          rval = rval.substr(0, candidate) +
            '\r\n' + insert + rval.substr(candidate + 1);
        }
        length = (i - candidate - 1);
        candidate = -1;
        ++i;
      } else if(rval[i] === ' ' || rval[i] === '\t' || rval[i] === ',') {
        candidate = i;
      }
    }
  
    return rval;
  }

  test();